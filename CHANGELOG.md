
# Changelog for social util library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.7.3] - 2019-12-19

[#18356] Bug Fixed: Social networking ... error in parsing URLs ending with "\)"

## [v1.7.2] - 2019-12-19

[#18122] Bug Fixed: Social Util Library parsing exceptions

[#17811] Bug Fixed: Fixed Avoid # being considered an hastagged topic

[#17844] Bug Fixed: Hashtag does not support numbers e.g. #4.15.0


## [v1.6.0] - 2019-03-29

[#13207] Revised social networking library mechanism for http links, mentions and hashtags recognition


## [v1.3.0] - 2017-12-31

[#10700] Fixed regex for hashtags

[#10984] Fixed regex for urls 

## [v1.2.1] - 2017-11-13

[#10234] Fixed regex for urls

## [v1.2.0] - 2017-04-13

[#19709] Email Templates library to support a new template or invitation to join VREs

Added method to remove html tags from a text (useful for #247)

## [v1.0.1] - 2016-10-01

[#4937] Hashtag regular expression updated

URL regular expression updated


## [v1.0.0] - 2016-06-01

First release 
